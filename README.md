# Soundcloud Desktop

Electron wrapper for soundcloud.com

### Windows

Latest Build: [![Build status](https://ci.appveyor.com/api/projects/status/coe8ne0a2lipnhiv?svg=true)](https://ci.appveyor.com/project/dfoord/soundcloud-desktop)

Master Branch: [![Build status](https://ci.appveyor.com/api/projects/status/coe8ne0a2lipnhiv/branch/master?svg=true)](https://ci.appveyor.com/project/dfoord/soundcloud-desktop/branch/master)

### Linux

Development Branch: [![CircleCI](https://circleci.com/bb/dfoord/soundcloud-desktop/tree/develop.svg?style=svg)](https://circleci.com/bb/dfoord/soundcloud-desktop/tree/develop)

Master Brach: [![CircleCI](https://circleci.com/bb/dfoord/soundcloud-desktop/tree/master.svg?style=svg)](https://circleci.com/bb/dfoord/soundcloud-desktop/tree/master)

![Capture.PNG](https://bitbucket.org/repo/RB78xd/images/3823338446-Capture.PNG)

## Purpose
Listen to your favorite tunes on soundcloud.com without having to open a RAM whore browser.

## Development

To get started developing, clone the repo and install the required dev dependencies.

- [NodeJS](http://nodejs.org/)
- [Electron](http://electron.atom.io/) `npm install -g electron-prebuilt`

Run a `npm install` in the repository root to install the application dependencies,
then `electron .` to run the application.

## Testing

Testing is done with [spectron](https://github.com/electron/spectron) and [mocha](https://github.com/mochajs/mocha).

To run the tests, run `npm test` in the repository root.

## Packaging
To pack the application into an asar archive (packages read by Electron) enter `npm install -g asar` to install the asar archiver. To build the archive use `asar pack <Project_Folder> app.asar.`

You should no be able to drop the `app.asar` archive into the resources folder of an Electron build and run the application.

## Platform buils

At the moment there are only Windows builds, Linux and iOS builds will come soon:
 
 - Windows x64 & x86 : `npm run build`
 - Windows 64 bit :    `npm run build-windows-x64`
 - Windows 32 bit :    `npm run build-windows-x86`
