'use strict';

const webview = require('./resources/js/webview');
const navButtons = require('./resources/js/nav-buttons');

webview.initialize((wvInstance) => {
    navButtons.bindEventListeners(wvInstance);
});