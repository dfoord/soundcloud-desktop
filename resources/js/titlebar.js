(($) => {

  'use strict';

  const {ipcRenderer} = require('electron')

  var btnMin = $('#title-minimize');
  var btnMax = $('#title-maximize');
  var btnClose = $('#title-close');

  btnMin.on('click', () => {
    ipcRenderer.send('titlebar-minimize');
  });

  btnMax.on('click', () => {
    ipcRenderer.send('titlebar-maximize');
  });

  ipcRenderer.on('titlebar-maximize-callback', (event, isMaximized) => {
    if(isMaximized) {
      btnMax.html('<i class="fa fa-compress fa-lg"></i>');
    } else {
      btnMax.html('<i class="fa fa-expand fa-lg"></i>');
    }
  });

  btnClose.on('click', () => {
    ipcRenderer.send('titlebar-close');
  });

})(require('jquery'));
