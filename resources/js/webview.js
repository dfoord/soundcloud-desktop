'use strict';

const navButtons = require('./nav-buttons');
const activityLoader = require('./activity-loader');
const container = require('./container');
const appLoader = require('./app-loader');  

var webview = document.getElementById('webview');

module.exports = {
  initialize: (done) => {

    webview.addEventListener('did-start-loading', () => {
      console.debug('webview:did-start-loading');
      if(webview.isLoading()) {
        activityLoader.instance.show();
      }
    });

    webview.addEventListener('did-stop-loading', () => {
      console.debug('webview:did-stop-loading');
      if(!webview.isLoading()) {
        activityLoader.instance.hide();
      }
    });

    webview.addEventListener('did-fail-load', () => {
      console.error('Failed to load', event);
      if(!webview.isLoading()) {
        activityLoader.instance.hide();
      }
    });

    webview.addEventListener('dom-ready', () => {

      console.debug('webview:dom-ready');
      
      // webview.openDevTools();

      webview.addEventListener('did-navigate', () => {
        console.debug('webview:did-navigate');
        navButtons.render(webview);
      });

      webview.addEventListener('did-navigate-in-page', () => {
        console.debug('webview:did-navigate-in-page');
        navButtons.render(webview);
      });

      var scrollbarCss = `
        ::-webkit-scrollbar { width: 8px; height: 8px; background: #CCC; }
        ::-webkit-scrollbar-thumb { background: #FF3A00; }
        ::-webkit-scrollbar-thumb:window-inactive { background: #FF3A00; }` ;

      webview.insertCSS(scrollbarCss);
      
      webview.addEventListener('ipc-message', (event) => {
        console.log(event.channel)
        // Prints "pong"
      });
      
      // TODO: Move this to an appropraite location. 
      // Need to implements a class that communicates with the guest page
      webview.executeJavaScript(`
        const {ipcRenderer} = require('electron')
        ipcRenderer.on('ping', () => {
          ipcRenderer.sendToHost('pong')
        });
      `, false, (result) => {});
      
      webview.send('ping');
        
      setTimeout(() => {
        container.instance.removeClass('hide');
        appLoader.instance.addClass('hidden');

        done(webview);
      });
      
    });

  }
};