'use strict';

const $          = require('jquery');
var navbuttons   = $('.nav-button');
var navbtnCntrs  = $('.nav-container');
var btnBack      = $('#btnBack');
var btnFwd       = $('#btnFwd');

module.exports = {
  instance: {
    navbuttons: navbuttons,
    navbtnCntrs: navbtnCntrs,
    btnBack: btnBack,
    btnFwd: btnFwd
  },
  bindEventListeners: (webview) => {
    
    btnBack.on('click', () => {
      webview.goBack();
      console.log('go back');
    });

    btnFwd.on('click', () => {
      webview.goForward();
      console.log('go back');
    });

  },
  render: (webview) => {
    
    var backBtn = $(navbtnCntrs[0]);
    var fwdBtn =  $(navbtnCntrs[1]);

    if(webview.canGoBack()) {
      backBtn.animate({ 'width': '65px' }, 'fast' );
    } else if(!backBtn.hasClass('hidden')) {
      backBtn.animate({ 'width': '0' }, 'fast' );
    }

    if(webview.canGoForward()) {
      fwdBtn.animate({ 'width': '65px' }, 'fast' );
    } else if(!fwdBtn.hasClass('hidden')) {
      fwdBtn.animate({ 'width': '0' }, 'fast' );
    }

  }
};