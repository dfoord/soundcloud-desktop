const exec = require('child_process').exec;

var cmd = `electron-packager . --overwrite --asar --platform=win32 --arch=${process.arch} --out=./out --icon=resources/img/soundcloud.ico --version=1.3.4 --win32metadata.ProductName=\"Sound Cloud Desktop\" --win32metadata.FileDescription=\"Sound Cloud Desktop Player\" --win32metadata.FileDescription=\"soundcloud-desktop.exe\"`;

console.log(cmd);

exec(cmd, (error, stdout, stderr) => {
  if (error) {
    console.error(`exec error: ${error}`);
    return;
  }
  console.log(`stdout: ${stdout}`);
  console.log(`stderr: ${stderr}`);
});
