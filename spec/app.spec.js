var Application = require('spectron').Application
var assert = require('assert')
var os = require('os')

describe('application launch', function () {
  
  this.timeout(10000)
  
  beforeEach(function () {
    
    var path;
    switch(os.platform()) {
      case 'win32':
        path = './node_modules/electron-prebuilt/dist/electron.exe'
        break;
      case 'linux':
        path = './node_modules/electron-prebuilt/dist/electron'
        break;
      case 'freebsd':
        path = './node_modules/electron-prebuilt/dist/electron'
        break;
      case 'darwin':
        path = './node_modules/electron-prebuilt/dist/Electron.app/Contents/MacOS/Electron'
        break;
      default:
        throw new Error('OS not recognized')
    }
    
    this.app = new Application({
      path: path,
      args: ['.']
    })
    return this.app.start()
  })

  afterEach(function () {
    if (this.app && this.app.isRunning()) {
      return this.app.stop()
    }
  })

  it('opens an initial window', function () {
    return this.app.client.getWindowCount().then(function (count) {
      assert.equal(count, 2); //This is 2 windows because of the embedded <webview/>
    })
  })
  
  it('shows an initial window', function () {
    return this.app.browserWindow.isVisible().then(function (visible) {
      assert.equal(visible, true); 
    })
  })
  
})