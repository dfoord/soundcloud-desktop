const electron = require('electron');
// Module to control application life.
const {app, Tray, Menu} = electron;
// Module to create native browser window.
const {BrowserWindow} = electron;

const {ipcMain} = require('electron')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let tray;

let iconPath = `${__dirname}/resources/img/soundcloud-icon.png`;
if (process.platform === 'windows') {
  iconPath = `${__dirname}/resources/img/soundcloud.ico`;
}

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    title: 'Soundcloud Desktop',
    width: 1174,
    height: 768,
    minWidth : 800,
    minHeight: 600,
    icon: iconPath,
    frame: false});

  // and load the index.html of the app.
  win.loadURL(`file://${__dirname}/index.html`);

  // Open the DevTools.
  // win.webContents.openDevTools();

  win.on('maximize', () => {
    win.webContents.send('titlebar-maximize-callback', win.isMaximized());
  });

  win.on('unmaximize', () => {
    win.webContents.send('titlebar-maximize-callback', win.isMaximized());
  });

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });
  
  tray = new Tray(iconPath);
  
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show/Hide',
      click() {
        if(win.isVisible()) {
          win.hide();
        } else {
          win.show();
        }
      } 
    },
    { label: 'Play/Pause', position: 'endof=actions' },
    { label: 'Previous Track', position: 'endof=actions' },
    { label: 'Next Track', position: 'endof=actions' },
    { 
      label: 'Quit', 
      position: 'endof=quit', 
      click() {
        app.quit();
      } 
    }
  ]);
  
  tray.setToolTip('Soundcloud Desktop');
  tray.setContextMenu(contextMenu);
  
  tray.on('double-click', () => {
    if(win.isVisible()) {
      win.hide();
    } else {
      win.show();
    }
  });

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.hide();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

ipcMain.on('titlebar-minimize', (event, arg) => {
  if(!win.isMinimized()){
    win.minimize();
  }
});

ipcMain.on('titlebar-maximize', (event, arg) => {
  if(!win.isMaximized()){
    win.maximize();
  } else if (win.isMaximized()) {
    win.restore();
  }

  event.sender.send('titlebar-maximize-callback', win.isMaximized());
});

ipcMain.on('titlebar-close', (event, arg) => {
  win.hide();
});
